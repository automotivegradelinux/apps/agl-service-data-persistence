--[[
   Copyright (C) 2018 "IoT.bzh"
   Author Frédéric Marec <frederic.marec@iot.bzh>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   NOTE: strict mode: every global variables should be prefixed by '_'
--]]

local testPrefix ="persistence_BasicAPITest_"

-- This tests the 'insert' verb of the persistence API
_AFT.describe(testPrefix.."insert_with_verification",function()
  _AFT.assertVerbStatusSuccess("persistence", "insert", {key="TESTinsert", value="insert"})
  _AFT.assertVerbResponseEquals("persistence","read", {key="TESTinsert"},{key="TESTinsert", value="insert"})
  _AFT.assertVerbStatusSuccess("persistence", "delete", {key="TESTinsert"})
  _AFT.assertVerbResponseEqualsError("persistence","read", {key="TESTinsert"},{key="TESTinsert", value="insert"})
end)

-- This tests the 'update' verb of the persistence API
_AFT.describe(testPrefix.."update_with_verification",function()
  _AFT.assertVerbStatusSuccess("persistence", "insert", {key="TESTupdate", value="myvalue"})
  _AFT.assertVerbResponseEquals("persistence","read", {key="TESTupdate"},{key="TESTupdate", value="myvalue"})
  _AFT.assertVerbStatusSuccess("persistence", "update", {key="TESTupdate", value="newvalue"})
  _AFT.assertVerbResponseEquals("persistence","read", {key="TESTupdate"},{key="TESTupdate", value="newvalue"})
  _AFT.assertVerbStatusSuccess("persistence", "delete", {key="TESTupdate"})
  _AFT.assertVerbResponseEqualsError("persistence","read", {key="TESTupdate"},{key="TESTupdate", value="myvalue"})
  _AFT.assertVerbResponseEqualsError("persistence","read", {key="TESTupdate"},{key="TESTupdate", value="newvalue"})
end)

-- This tests the 'delete' verb of the persistence API
_AFT.describe(testPrefix.."delete_with_verification",function()
  _AFT.assertVerbStatusSuccess("persistence", "insert", {key="TESTdelete", value="delete"})
  _AFT.assertVerbResponseEquals("persistence","read", {key="TESTdelete"},{key="TESTdelete", value="delete"})
  _AFT.assertVerbStatusSuccess("persistence", "delete", {key="TESTdelete"})
  _AFT.assertVerbResponseEqualsError("persistence","read", {key="TESTdelete"},{key="TESTdelete", value="delete"})
end)

-- This tests the 'insert' verb of the persistence API
_AFT.testVerbStatusSuccess(testPrefix.."insert","persistence","insert", {key="TESTinsert", value="insert"}, nil,
  function()
    _AFT.assertVerbStatusSuccess("persistence", "delete", {key="TESTinsert"})
  end)

-- This tests the 'delete' verb of the persistence API
_AFT.testVerbStatusSuccess(testPrefix.."delete","persistence","delete", {key="TESTdelete"},
  function()
    _AFT.assertVerbStatusSuccess("persistence", "insert", {key="TESTdelete", value="delete"})
  end, nil)

-- This tests the 'update' verb of the persistence API
_AFT.testVerbStatusSuccess(testPrefix.."update","persistence","update", {key="TESTupdate", value="testupdate"},
  function()
    _AFT.assertVerbStatusSuccess("persistence", "insert", {key="TESTupdate", value="newupdate"})
  end,
  function()
    _AFT.assertVerbStatusSuccess("persistence", "delete", {key="TESTupdate"})
  end)

-- This tests the 'read' verb of the persistence API
_AFT.testVerbStatusSuccess(testPrefix.."read","persistence","read", {key="TESTread"},
  function()
    _AFT.assertVerbStatusSuccess("persistence", "insert", {key="TESTread", value="myvalue"})
  end,
  function()
    _AFT.assertVerbStatusSuccess("persistence", "delete", {key="TESTread"})
  end)

-- This tests the 'read' verb of the persistence API
_AFT.testVerbStatusError(testPrefix.."read-unknow-value","persistence","read", {key="TESTread-unknow-value"}, nil, nil)

-- This tests the 'insert' verb of the persistence API
_AFT.testVerbStatusError(testPrefix.."insert-without-value","persistence","insert", {key="TESTread-unknow-value"}, nil,
  function()
    _AFT.assertVerbStatusError("persistence", "delete", {key="TEST"})
  end)

-- This tests the 'insert' verb of the persistence API
_AFT.testVerbStatusError(testPrefix.."insert-with-wrong_key","persistence","insert", {wrong_key="TESTinsert-wrong-key", value="TESTinsert-wrong-key"}, nil,
  function()
    _AFT.assertVerbStatusError("persistence", "delete", {key="TESTinsert-wrong-key"})
  end)

-- This tests the 'insert' verb of the persistence API
_AFT.testVerbStatusError(testPrefix.."insert-with-wrong_value","persistence","insert", {key="TESTinsert-wrong-value", wrong_value="TESTinsert-wrong-value"}, nil,
  function()
    _AFT.assertVerbStatusError("persistence", "delete", {key="TESTinsert-wrong-value"})
  end)

-- This tests the 'delete' verb of the persistence API
_AFT.testVerbStatusError(testPrefix.."delete-unknow-key","persistence","delete", {key="TESTdelete-unknow-key"}, nil, nil)

-- This tests the 'insert' verb of the persistence API
_AFT.testVerbStatusError(testPrefix.."insert-with-anything","persistence","insert", {value="TESTinsert-with-anything"}, nil, nil)

-- This tests the 'update' verb of the persistence API
_AFT.testVerbStatusError(testPrefix.."update-unkwon-null-value","persistence","update", {key=null}, nil, nil)
