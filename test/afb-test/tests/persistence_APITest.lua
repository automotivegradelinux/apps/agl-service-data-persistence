--[[
   Copyright (C) 2019 "IoT.bzh"
   Author Frédéric Marec <frederic.marec@iot.bzh>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   NOTE: strict mode: every global variables should be prefixed by '_'
--]]

local testPrefix ="persistence_APITest_"
local key_values0 = {"TESTinsert", "insert", "wrong_value", 3.1415926546, nil}
local key_values1 = key_values0
local key_values2 = key_values0

local function formatize (arg)
  return tostring(arg):gsub("[^a-zA-Z0-9_]", "_")
end

for _, key_value0 in pairs(key_values0) do
  for _, key_value1 in pairs(key_values1) do

    local arg1 = "key_"..formatize(key_value0).."_"
    local arg2 = "value_"..formatize(key_value1)

    -- This tests the 'insert' verb of the persistence API
    local test_name_insert = testPrefix.."insert_with_verification_"..arg1..arg2
    _AFT.describe(test_name_insert,function()
      _AFT.assertVerbStatusSuccess("persistence", "insert", {key= key_value0, value= key_value1})
      _AFT.assertVerbResponseEquals("persistence","read", {key= key_value0},{key= key_value0, value= key_value1})
      _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value0})
    end)

    -- This tests the 'delete' verb of the persistence API
    local test_name_deldete = testPrefix.."delete_with_verification_"..arg1..arg2
    _AFT.describe(test_name_deldete,function()
      _AFT.assertVerbStatusSuccess("persistence", "insert", {key= key_value0, value= key_value1})
      _AFT.assertVerbResponseEquals("persistence","read", {key= key_value0},{key= key_value0, value= key_value1})
      _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value0})
    end)

    for _, key_value2 in pairs(key_values2) do

      local arg3 = "_updated_value_"..formatize(key_value2)

      -- This tests the 'update' verb of the persistence API
      local test_name_update = testPrefix.."update_with_verification_"..arg1..arg2..arg3
      _AFT.describe(test_name_update,function()
        _AFT.assertVerbStatusSuccess("persistence", "insert", {key= key_value0, value= key_value1})
        _AFT.assertVerbResponseEquals("persistence","read", {key= key_value0},{key= key_value0, value= key_value1})
        _AFT.assertVerbStatusSuccess("persistence", "update", {key= key_value0, value= key_value2})
        _AFT.assertVerbResponseEquals("persistence","read", {key= key_value0},{key= key_value0, value= key_value2})
        _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value0})
        _AFT.assertVerbResponseEqualsError("persistence","read", {key= key_value0},{key= key_value0, value= key_value2})
        _AFT.assertVerbResponseEqualsError("persistence","read", {key= key_value0},{key= key_value0, value= key_value1})
      end)

      -- This tests 'multiple insert' of the persistence API
      local test_name_multiple_insert = testPrefix.."multiple_insert_with_verification_"..arg1..arg2..arg3
      _AFT.describe(test_name_multiple_insert,function()
        
        if ((key_value0 == key_value2) or (key_value1 == key_value2) or (key_value0 == key_value1)
          or ((key_value0 == key_value2) and (key_value1 == key_value2) and (key_value0 == key_value1)))
        then
          _AFT.assertVerbStatusSuccess("persistence", "update", {key= key_value2, value= key_value2})
 
          if (key_value0 == key_value2)
          then
            _AFT.assertVerbStatusError("persistence", "insert", {key= key_value0, value= key_value0})
          else
            _AFT.assertVerbStatusSuccess("persistence", "insert", {key= key_value0, value= key_value0})
          end

          if (key_value1 == key_value2) or (key_value0 == key_value1)
          then
            _AFT.assertVerbStatusError("persistence", "insert", {key= key_value1, value= key_value1})
          else
            _AFT.assertVerbStatusSuccess("persistence", "insert", {key= key_value1, value= key_value1})
          end

          _AFT.assertVerbStatusError("persistence", "insert", {key= key_value1, value= key_value2})
          _AFT.assertVerbStatusError("persistence", "insert", {key= key_value2, value= key_value0})
          _AFT.assertVerbResponseEquals("persistence","read", {key= key_value0},{key= key_value0, value= key_value0})
          _AFT.assertVerbResponseEquals("persistence","read", {key= key_value1},{key= key_value1, value= key_value1})
          _AFT.assertVerbResponseEquals("persistence","read", {key= key_value2},{key= key_value2, value= key_value2})

          if (key_value0 == key_value2)
          then
            _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value0})
            _AFT.assertVerbStatusError("persistence", "delete", {key= key_value2})
          else
            _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value0})
            _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value2})
          end

          if (key_value1 == key_value2) or (key_value0 == key_value1)
          then
            _AFT.assertVerbStatusError("persistence", "delete", {key= key_value1})
          else
            _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value1})
          end

        else -- key_values0, key_value1 & key_value2 not equal
          _AFT.assertVerbStatusSuccess("persistence", "update", {key= key_value2, value= key_value2})
          _AFT.assertVerbStatusSuccess("persistence", "insert", {key= key_value0, value= key_value0})
          _AFT.assertVerbStatusSuccess("persistence", "insert", {key= key_value1, value= key_value1})
          _AFT.assertVerbStatusError("persistence", "insert", {key= key_value1, value= key_value2})
          _AFT.assertVerbStatusError("persistence", "insert", {key= key_value2, value= key_value0})
          _AFT.assertVerbResponseEquals("persistence","read", {key= key_value0},{key= key_value0, value= key_value0})
          _AFT.assertVerbResponseEquals("persistence","read", {key= key_value1},{key= key_value1, value= key_value1})
          _AFT.assertVerbResponseEquals("persistence","read", {key= key_value2},{key= key_value2, value= key_value2})
          _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value0})
          _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value1})
          _AFT.assertVerbStatusSuccess("persistence", "delete", {key= key_value2})
        end
      end)
    end
  end
end
